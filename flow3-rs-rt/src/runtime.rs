use embassy_executor::{Executor, Spawner, SpawnToken};
use esp_println::println;
use hal::{i2c::I2C, peripherals::I2C0};
use shared_bus::{I2cProxy, XtensaMutex};
use static_cell::StaticCell;

use flow3_rs::{Flow3r, input::InputRunner, captouch::CaptouchRunner};

use crate::flow3r::init_flow3r;

pub static EXECUTOR: StaticCell<Executor> = StaticCell::new();


/// This function starts the async runtime, initializes the hardware and then starts the provided `main` function.
/// The function passed as an argument must be an embassy-task.
/// 
/// ```rust
/// #[entry]
/// fn runtime() -> ! {
///     start_runtime(main)
/// }
/// 
/// #[embassy_runtime::task]
/// async fn main(flow3r: Flow3r) {
///     // do something
/// }
/// ```
pub fn start_runtime<S>(main: fn(Flow3r) -> SpawnToken<S>) -> ! 
{
    println!("starting runtime");
    let main: fn(Flow3r) -> SpawnToken<*mut ()> = unsafe { core::mem::transmute(main) };
    let executor = EXECUTOR.init(Executor::new());
    executor.run(|spawner| {
        spawner.spawn(init_runtime(main)).unwrap();
    });
}


#[embassy_executor::task]
async fn init_runtime(main: fn(Flow3r) -> SpawnToken<*mut ()>) {
    let (flow3r, input_runner, captouch_runner) = init_flow3r().await;

    // Spawn background tasks
    let spawner = Spawner::for_current_executor().await;
    spawner
        .spawn(input_task(input_runner))
        .unwrap();
    spawner
        .spawn(captouch_task(captouch_runner))
        .unwrap();

    // Hand over to main task
    spawner.spawn(main(flow3r)).unwrap();
}

#[embassy_executor::task]
async fn input_task(runner: InputRunner) -> ! {
    runner.run().await
}

#[embassy_executor::task]
async fn captouch_task(runner: CaptouchRunner) -> ! {
    runner.run().await
}

#[embassy_executor::task]
async fn input(
    i2c: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>,
 ) {
    let mut pe2 = port_expander::Max7321::new(i2c, true, true, false, true);

    let pe2_io = pe2.split();
    let mut sw1_r = pe2_io.p0;
    sw1_r.set_high().unwrap();
    let mut sw1_l = pe2_io.p7;
    sw1_l.set_high().unwrap();
    let mut sw2_r = pe2_io.p5;
    sw2_r.set_high().unwrap();
    let mut sw2_l = pe2_io.p4;
    sw2_l.set_high().unwrap();

    let _lcd_rst = pe2_io.p3;
}