mod basic;
pub use basic::*;

use crate::rotation::DisplayRotation;
use embedded_hal::blocking::delay::DelayMs;

pub trait DisplayConfiguration<DELAY>
where
    DELAY: DelayMs<u8>,
{
    type Error;

    /// Set display rotation
    async fn set_rotation(&mut self, rotation: DisplayRotation) -> Result<(), Self::Error>;

    /// Initialize and configure the display for the given mode
    async fn init(&mut self, delay: &mut DELAY) -> Result<(), Self::Error>;
}
