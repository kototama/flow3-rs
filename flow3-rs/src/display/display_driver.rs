use embassy_time::Delay;
use gc9a01::{
    mode::BasicMode,
    prelude::{DisplayConfiguration, DisplayResolution240x240},
    rotation::DisplayRotation,
    Gc9a01,
};
use esp_hal::{
    dma::{Channel, DmaChannel0, ChannelCreator},
    gpio::GpioPin,
    peripherals::SPI2,
    spi::{
        master::{Spi, SpiDma},
        FullDuplexMode,
    },
    Async
};

use super::display_interface::SpiDmaInterface;

pub type DisplayDriver<'a> = Gc9a01<
    SpiDmaInterface<
        SpiDma<'a, SPI2, Channel<'a, DmaChannel0, Async>, FullDuplexMode, Async>,
        GpioPin<38>,
        GpioPin<40>,
    >,
    DisplayResolution240x240,
    BasicMode,
>;

pub async fn init_display_driver<'a>(
    spi: Spi<'static, SPI2, FullDuplexMode>,
    channel: ChannelCreator<0>,
    descriptors: &'a mut [u32; 27],
    rx_descriptors: &'a mut [u32; 27],
    dc: GpioPin<38>,
    cs: GpioPin<40>,
) -> DisplayDriver<'a> {
    let spi_dma = spi.with_dma(channel.configure_for_async(
        false,
        // descriptors,
        // rx_descriptors,
        esp_hal::dma::DmaPriority::Priority0,
    ));

    // TODO: use descriptors somehow?
    

    let display_interface = SpiDmaInterface::new(
        spi_dma,
        dc.into_push_pull_output(),
        cs.into_push_pull_output(),
    );

    let mut display_driver = Gc9a01::new(
        display_interface,
        DisplayResolution240x240,
        DisplayRotation::Rotate180,
    );

    display_driver.init(&mut Delay).await.unwrap();

    display_driver
}
