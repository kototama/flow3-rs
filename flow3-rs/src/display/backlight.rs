use esp_hal::gpio::GpioPin;
use esp_hal::ledc::channel::Channel;
use esp_hal::ledc::timer::Timer;
use esp_hal::ledc::{LSGlobalClkSource, LowSpeed, Ledc};
// use embedded_hal::prelude::*;
use static_cell::StaticCell;

pub type Backlight = Channel<'static, LowSpeed, GpioPin<46>>;

static LEDC: StaticCell<Ledc<'static>> = StaticCell::new();
static LSTIMER0: StaticCell<Timer<'static, LowSpeed>> = StaticCell::new();

pub fn init_backlight(ledc: Ledc<'static>, bl: GpioPin<46>) -> Backlight {
    let ledc = LEDC.init(ledc);

    ledc.set_global_slow_clock(LSGlobalClkSource::APBClk);

    let lstimer0 = LSTIMER0.init(ledc.get_timer::<LowSpeed>(esp_hal::ledc::timer::Number::Timer0));
    lstimer0
        .configure(esp_hal::ledc::timer::config::Config {
            duty: esp_hal::ledc::timer::config::Duty::Duty5Bit,
            clock_source: esp_hal::ledc::timer::LSClockSource::APBClk,
            frequency: 24u32.kHz(),
        })
        .unwrap();

    let mut channel0 = ledc.get_channel(esp_hal::ledc::channel::Number::Channel0, bl);
    channel0
        .configure(esp_hal::ledc::channel::config::Config {
            timer: lstimer0,
            duty_pct: 0,
            pin_config: esp_hal::ledc::channel::config::PinConfig::PushPull,
        })
        .unwrap();

    channel0
}
