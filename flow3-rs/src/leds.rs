use esp_hal_smartled::{smartLedBuffer, SmartLedsAdapter};
use esp_hal::{
    gpio::GpioPin, rmt::Channel, rmt::Rmt,
};
use esp_hal::Blocking;

pub type Leds = SmartLedsAdapter<Channel<Blocking, 0>, 961>;

pub fn init_leds(rmt: Rmt<Blocking>, pin: GpioPin<14>) -> Leds {
    let rmt_buffer = smartLedBuffer!(40);
    SmartLedsAdapter::new(rmt.channel0, pin, rmt_buffer)
}

pub fn brightness_fade_in_out(brightness: (u8, bool), max: u8, min: u8) -> (u8, bool) {
    match brightness {
        (v, true) if v < max => (v + 1, true),
        (v, true) => (v - 1, false),
        (v, false) if v > min => (v - 1, false),
        (v, false) => (v + 1, true),
    }
}
