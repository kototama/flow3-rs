#![no_std]
#![feature(type_alias_impl_trait, async_fn_in_trait)]

//! This crate contains the board support for the [`Flow3r`]. It provides higher-level
//! abstractions for the hardware components of the flower.
//! 
//! This crate is written for an async runtime. There are some tasks that need to be running in the
//! background for all functionality to work. It was tested with embassy and the flow3-rs-rt, however
//! it _should_ be agnostic over the async runtime used.


use esp_hal::rng::Rng;

use self::badgelink::BadgeLink;
use self::badgelink::badgenet::{BadgenetUartLeft, BadgenetUartRight};
use self::captouch::Captouch;
use self::display::Display;
use self::imu::ImuHandler;
use self::input::Inputs;
use self::leds::Leds;

pub mod badgelink;
pub mod captouch;
pub mod display;
pub mod imu;
pub mod input;
pub mod leds;
pub mod sdcard;

unsafe impl Sync for Flow3r {}

/// This struct contains all the high-level peripherals of the board.
/// To use a peripheral, call the respective `take` or `get` function. This returns an owned version of that peripheral.
/// Each `take` function can only be called once, and will panic if called twice. 
/// The peripherals behind a `get` function can exist multiple times.
pub struct Flow3r {
    badgelink: Option<BadgeLink>,
    display: Option<Display>,
    imu: Option<ImuHandler>,
    leds: Option<Leds>,
    uart0: Option<BadgenetUartLeft>,
    uart1: Option<BadgenetUartRight>,
    rng: Option<Rng>,
}

impl Flow3r {
    pub fn new(
        badgelink: Option<BadgeLink>,
        display: Option<Display>,
        imu: Option<ImuHandler>,
        leds: Option<Leds>,
        uart0: Option<BadgenetUartLeft>,
        uart1: Option<BadgenetUartRight>,
        rng: Option<Rng>,
    ) -> Self {
        Self {
            badgelink,
            display,
            imu,
            leds,
            uart0,
            uart1,
            rng,
        }
    }

    pub fn take_badgelink(&mut self) -> BadgeLink {
        self.badgelink.take().expect("can only take badgelink once!")
    }

    pub fn get_captouch(&mut self) -> Captouch {
        Captouch
    }

    pub fn take_display(&mut self) -> Display {
        self.display.take().expect("can only take display once!")
    }

    pub fn get_inputs(&mut self) -> Inputs {
        Inputs
    }

    pub fn take_imu(&mut self) -> ImuHandler {
        self.imu.take().expect("can only take imu once!")
    }

    pub fn take_leds(&mut self) -> Leds {
        self.leds.take().expect("can only take leds once!")
    }

    pub fn take_uart_left(&mut self) -> BadgenetUartLeft {
        self.uart0.take().expect("can only take uart left once!")
    }

    pub fn take_uart_right(&mut self) -> BadgenetUartRight {
        self.uart1.take().expect("can only take uart right once!")
    }

    pub fn take_rng(&mut self) -> Rng {
        self.rng.take().expect("can only take uart rng once!")
    }
}